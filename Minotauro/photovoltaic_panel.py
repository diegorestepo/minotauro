'''
 * ------------------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
 * ------------------------------------------------------------------------------------
'''


import numpy as np


class PhotovoltaicPanel:
    '''Ortiz model for photovoltaic panel.'''

    def __init__(self, Vi, s, p, Ei, Ein, Tn, b, Isc, Voc, T, TCv, TCi):
        self.Vi = Vi
        self.s = s
        self.p = p
        self.Ei = Ei
        self.Ein = Ein
        self.Tn = Tn
        self.b = b
        self.Isc = Isc
        self.Voc = Voc
        self.T = T
        self.TCv = TCv
        self.TCi = TCi
        self.Vmax = self.Voc*1.03
        self.Vmin = self.Voc*0.5

    def current(self):
        self.Ix = self.p*(self.Ei/self.Ein)*(self.Isc +
                                             (self.TCi*(self.T - self.Tn)))
        return self.Ix

    def voltage(self):
        self.Vx = self.s*(self.Ei/self.Ein)*self.TCv*(self.T-self.Tn) + (self.s*self.Vmax) - (self.s*(
            self.Vmax - self.Vmin))*np.exp((self.Ei/self.Ein)*np.log((self.Vmax - self.Voc)/(self.Vmax - self.Vmin)))
        return self.Vx

    def model_init(self):
        self.current()
        self.voltage()

    def model_current(self):
        self.Iv = (self.Ix/(1 - np.exp((-1/self.b)))) * \
            (1-(np.exp((self.Vi/(self.b*self.Vx)) - (1/self.b))))
        return self.Iv

    def model_power(self):
        self.Pot = self.model_current()*self.Vi
        return self.Pot

    def panel_curves(self, step):
        self.Vi = step
        self.finc = self.voltage()/step
        self.model_init()

        self.list_Vi = []
        self.list_Iv = []
        self.list_Pot = []

        for i in range(int(self.finc)):
            self.Iv = self.model_current()
            self.Pot = self.model_power()

            self.list_Vi.append(self.Vi)
            self.list_Iv.append(self.Iv)
            self.list_Pot.append(self.Pot)

            self.Vi = self.Vi + step

        return self.list_Vi, self.list_Iv, self.list_Pot
