import plotext as plx
from Minotauro import WindTurbine as Turbine


def run():
    turbine = Turbine(cp=0.48,
                      rho=1.225,
                      area=1.0751,
                      velwind=0.0,
                      c1=0.5176,
                      c2=116,
                      c3=0.4,
                      c4=5,
                      c5=21,
                      c6=0.0068,
                      lambda0=0.0,
                      beta=0.0,
                      lambdai=1.0)
    step = 35
    limit = 25

    velwind, Pot = turbine.power_curve(step, limit)

    plx.scatter(velwind, Pot)
    plx.show()


if __name__ == '__main__':
    run()
