from Minotauro import GoldenSection as GSS
from Minotauro import parcial0, parcial1, parcial2, parcial3
from multiprocessing import Process


def GSS_parcial0():
    teseo = GSS(ax=0.01, bx=23, f=parcial0)
    xop, fop = teseo.search()

    print('\nParcial0: ')
    print('\nx = {xop} y f = {fop}\n'.format(xop=xop, fop=fop))


def GSS_parcial1():
    teseo = GSS(ax=0.01, bx=21.4, f=parcial1)
    xop, fop = teseo.search()

    print('\nParcial1: ')
    print('\nx = {xop} y f = {fop}\n'.format(xop=xop, fop=fop))


def GSS_parcial2():
    teseo = GSS(ax=0.01, bx=13.7, f=parcial2)
    xop, fop = teseo.search()

    print('\nParcial2: ')
    print('\nx = {xop} y f = {fop}\n'.format(xop=xop, fop=fop))


def GSS_parcial3():
    teseo = GSS(ax=0.01, bx=13.7, f=parcial3)
    xop, fop = teseo.search()

    print('\nParcial3: ')
    print('\nx = {xop} y f = {fop}\n'.format(xop=xop, fop=fop))


def run():
    GSS0 = Process(target=GSS_parcial0)
    GSS1 = Process(target=GSS_parcial1)
    GSS2 = Process(target=GSS_parcial2)
    GSS3 = Process(target=GSS_parcial3)

    GSS0.start()
    GSS1.start()
    GSS2.start()
    GSS3.start()


if __name__ == '__main__':
    run()
