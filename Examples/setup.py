from pathlib import Path


def run():
    Path('./Minotauro').symlink_to('../Minotauro') if not Path('Minotauro').exists() else None


if __name__ == '__main__':
    run()
