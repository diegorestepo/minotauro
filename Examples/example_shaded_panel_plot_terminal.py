import plotext as plx
import numpy as np
from Minotauro import PhotovoltaicPanel as PV


def _mayor(x1, x2, x3, x4, x5):
    y1 = len(x1)
    y2 = len(x2)
    y3 = len(x3)
    y4 = len(x4)
    y5 = len(x5)

    y = [y1, y2, y3, y4, y5]

    if max(y) == y1:
        return x1
    elif max(y) == y2:
        return x2
    elif max(y) == y3:
        return x3
    elif max(y) == y4:
        return x4
    elif max(y) == y5:
        return x5


def _complete(size, x):
    if size > len(x):
        dif = size - len(x)
        comp = [0 for i in range(dif)]
        comp = np.array(comp)

        return np.concatenate((x, comp), axis=0)
    else:
        return x


def run():
    panel1 = PV(Vi=0, s=1, p=1, Ei=1000, Ein=1000, Tn=25, b=0.0684,
                Isc=3.71, Voc=21.40, T=25, TCv=-0.1261, TCi=0.00418)
    panel2 = PV(Vi=0, s=1, p=1, Ei=1000, Ein=1000, Tn=25, b=0.0684,
                Isc=3.71, Voc=21.40, T=25, TCv=-0.1261, TCi=0.00418)
    panel3 = PV(Vi=0, s=1, p=1, Ei=1000, Ein=1000, Tn=25, b=0.0684,
                Isc=3.71, Voc=21.40, T=25, TCv=-0.1261, TCi=0.00418)
    panel4 = PV(Vi=0, s=1, p=1, Ei=1000, Ein=1000, Tn=25, b=0.0684,
                Isc=3.71, Voc=21.40, T=25, TCv=-0.1261, TCi=0.00418)
    panel5 = PV(Vi=0, s=1, p=1, Ei=1000, Ein=1000, Tn=25, b=0.0684,
                Isc=3.71, Voc=21.40, T=25, TCv=-0.1261, TCi=0.00418)

    step = 0.01

    panel1.Ei = 10
    panel1.T = 5

    panel2.Ei = 100
    panel2.T = 45

    panel3.Ei = 100
    panel3.T = 10

    panel4.Ei = 500
    panel4.T = 150

    panel5.Ei = 700
    panel5.T = 150

    Vi1, Iv1, Pot1 = panel1.panel_curves(step)
    Vi2, Iv2, Pot2 = panel2.panel_curves(step)
    Vi3, Iv3, Pot3 = panel3.panel_curves(step)
    Vi4, Iv4, Pot4 = panel4.panel_curves(step)
    Vi5, Iv5, Pot5 = panel5.panel_curves(step)

    Vi1 = np.array(Vi1)
    Vi2 = np.array(Vi2)
    Vi3 = np.array(Vi3)
    Vi4 = np.array(Vi4)
    Vi5 = np.array(Vi5)

    Pot1 = np.array(Pot1)
    Pot2 = np.array(Pot2)
    Pot3 = np.array(Pot3)
    Pot4 = np.array(Pot4)
    Pot5 = np.array(Pot5)

    Vi = _mayor(Vi1, Vi2, Vi3, Vi4, Vi5)
    Pot = _mayor(Pot1, Pot2, Pot3, Pot4, Pot5)
    size = len(Pot)

    Pot1x = _complete(size, Pot1)
    Pot2x = _complete(size, Pot2)
    Pot3x = _complete(size, Pot3)
    Pot4x = _complete(size, Pot4)
    Pot5x = _complete(size, Pot5)

    Pot = Pot1x + Pot2x + Pot3x + Pot4x + Pot5x

    plx.plot(Vi1, Pot1)
    plx.plot(Vi2, Pot2)
    plx.plot(Vi3, Pot3)
    plx.plot(Vi4, Pot4)
    plx.plot(Vi5, Pot5)
    plx.plot(Vi, Pot)
    plx.show()


if __name__ == '__main__':
    run()
