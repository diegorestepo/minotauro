from Minotauro import GoldenSection as GSS
from Minotauro import parcial0 as parcial


def run():
    teseo = GSS(ax=0.01, bx=23, f=parcial)
    xop, fop = teseo.search()

    print(f'\nx = {xop} y f = {fop}\n')


if __name__ == '__main__':
    run()
