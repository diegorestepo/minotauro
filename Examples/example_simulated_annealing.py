from Minotauro import SimulatedAnnealing as SA
from Minotauro import parcial0 as parcial


def run():
    teseo = SA(T=0.5, Tmin=0.1, tol=2, alfa=0.5, f=parcial, rank=23.0)
    Vi, Pi = teseo.search()

    print(f'\nx = {Vi} y f = {Pi}\n')


if __name__ == '__main__':
    run()
