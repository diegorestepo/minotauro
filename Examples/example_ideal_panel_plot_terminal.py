import plotext as plx
from Minotauro import PhotovoltaicPanel as PV


def run():
    panel = PV(Vi=0,
               s=1,
               p=1,
               Ei=1000,
               Ein=1000,
               Tn=25,
               b=0.0684,
               Isc=3.71,
               Voc=21.40,
               T=25,
               TCv=-0.1261,
               TCi=0.00418)

    step = 0.01

    Vi, Iv, Pot = panel.panel_curves(step)

    plx.plot(Vi, Pot)
    plx.show()


if __name__ == '__main__':
    run()
