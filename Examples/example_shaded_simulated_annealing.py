from Minotauro import SimulatedAnnealing as SA
from Minotauro import parcial0, parcial1, parcial2, parcial3
from multiprocessing import Process


def SA_parcial0_T_05():
    teseo = SA(T=0.5, Tmin=0.1, tol=2, alfa=0.5, f=parcial0, rank=23.0)
    Vi, Pi = teseo.search()

    print('\nParcial0, T = 0.5: ')
    print(f'\nx = {Vi} y f = {Pi}\n')


def SA_parcial1_T_05():
    teseo = SA(T=0.5, Tmin=0.1, tol=2, alfa=0.5, f=parcial1, rank=21.4)
    Vi, Pi = teseo.search()

    print('\nParcial1, T = 0.5: ')
    print(f'\nx = {Vi} y f = {Pi}\n')


def SA_parcial2_T_05():
    teseo = SA(T=0.5, Tmin=0.1, tol=2, alfa=0.5, f=parcial2, rank=13.7)
    Vi, Pi = teseo.search()

    print('\nParcial2, T = 0.5: ')
    print(f'\nx = {Vi} y f = {Pi}\n')


def SA_parcial3_T_05():
    teseo = SA(T=0.5, Tmin=0.1, tol=2, alfa=0.5, f=parcial3, rank=13.7)
    Vi, Pi = teseo.search()

    print('\nParcial3, T = 0.5: ')
    print(f'\nx = {Vi} y f = {Pi}\n')


def run():
    SA0T05 = Process(target=SA_parcial0_T_05)
    SA1T05 = Process(target=SA_parcial1_T_05)
    SA2T05 = Process(target=SA_parcial2_T_05)
    SA3T05 = Process(target=SA_parcial3_T_05)

    SA0T05.start()
    SA1T05.start()
    SA2T05.start()
    SA3T05.start()


if __name__ == '__main__':
    run()
